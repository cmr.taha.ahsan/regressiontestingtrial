from selenium import webdriver
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import Login

# Constants defined on top, change testing bin name here
binName = "Test Jerry"
chromePath = "chromedriver/chromedriver.exe"
firefoxPath = "geckodriver/geckodriver.exe"
chromeProfile = r"--user-data-dir=C:\Users\taha.ahsan\AppData\Local\Google\Chrome\User Data"
firefoxProfile = r"C:\Users\taha.ahsan\AppData\Roaming\Mozilla\Firefox\Profiles\wdyqlvps.default-release"


driver = Login.loginFirefox(firefoxPath, firefoxProfile)

# Searched for the Bins page and presses the Enter key
driver.find_element(By.ID, "_searchstring").send_keys("Page: Bins")
driver.find_element(By.ID, "_searchstring").send_keys(Keys.ENTER)

# Clicks on "New Bin" link
driver.find_element(By.CSS_SELECTOR, "input[value='New Bins']").click()

# Enters the bin number into the correct field
driver.find_element(By.NAME, "binnumber").send_keys(binName)

# Enters the location
driver.find_element(By.NAME, "location_display").send_keys("ARE DUB 02")
driver.find_element(By.NAME, "location_display").send_keys(Keys.ENTER)

# Enters the memo
driver.find_element(By.NAME, "memo").send_keys("This is a selenium memo")

# Creates the new bin
driver.find_element(By.ID, "btn_multibutton_submitter").click()

"""
Checking Starts Here
"""

# Clicks on "Lists" on the nav bar
driver.find_element(By.XPATH, "//li[@data-title='Lists']").click()

# Finds all the links that the user can click on
listLinks = driver.find_elements(By.XPATH, "//div[@class='ns-link-category-title ns-link-category-title-link ']")

# Checks to see if one of the links is called "Supply Chain", checks to see if it's expanded, if not, clicks on it
for link in listLinks:
    # print(link.text)
    if link.text == "Supply Chain":
        parent = link.find_element(By.XPATH, "parent::div")
        print(parent.get_attribute("data-expanded"))
        if parent.get_attribute("data-expanded") == "false":
            link.click()

# Starts the actions chain, allows for certain functionality
action = ActionChains(driver)

# Finds the Bins link under "Supply Chain" and moves the mouse to hover over the link
binsLink = driver.find_element(By.XPATH, "//a[text()='Bins']")
action.move_to_element(binsLink).perform()

# Clicks on "Search" next to the "Bins" link (only visible when hovering)
driver.find_element(By.XPATH, "//a[text()='Bins']/parent::div/div/a[text()='Search']").click()

# Makes sure that the user is in normal search, not advanced
checkbox = driver.find_element(By.XPATH, "//input[@id='advanced_fs_inp']")
if checkbox.is_selected():
    checkbox.click()

# Finds the dropdown and selects "Starts With" for searching bin numbers
driver.find_element(By.XPATH, "//input[@name='inpt_BinNumber_BINNUMBERtype']").click()
driver.find_element(By.XPATH, "//div[text()='starts with']").click()

# Enters the text that the bin number should start with
driver.find_element(By.XPATH, "//input[@name='BinNumber_BINNUMBER']").send_keys("Test")

# Executes the search
driver.find_element(By.XPATH, "//input[@id='submitter']").click()

# Collects the bin number from every row and adds it to an array
rows = driver.find_elements(By.XPATH, "//table[@id='div__bodytab']/tbody/tr/td[2]")
bins = []
for row in rows:
    bins.append(row.text)

# Checks to see if the bin name is in the bins array
if binName in bins:
    print("The bin was successfully created")

    # Collects all the edit links
    editLinks = driver.find_elements(By.XPATH, "//table[@id='div__bodytab']/tbody/tr/td[1]/a[1]")
    for link in editLinks:
        # Looks at the bin number of the respective link to see if it matches the bin name
        linkBin = link.find_element(By.XPATH, "parent::td/parent::tr/td[2]").text

        if linkBin == binName:
            # Click on the edit link
            link.click()

            # Finds the "Actions" link on the page, and hovers over it
            actionLink = driver.find_element(By.CLASS_NAME, "pgm_action_menu")
            action.move_to_element(actionLink).perform()

            # Clicks on the "Delete" link (only visible)
            driver.find_element(By.CLASS_NAME, "ddmAnchor").click()

            # Switch to the alert that pops up, make sure that the text is right, accept the alert
            alert = driver.switch_to.alert
            assert "Are you sure you want to delete this record?" == alert.text
            alert.accept()
            print("Test bin has successfully deleted ")

            # End the loop
            break

else:
    print("The bin was not created")
