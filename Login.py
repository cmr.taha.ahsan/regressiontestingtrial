from selenium import webdriver
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


def loginChrome(path, profile):
    # This loads your chrome profile to avoid 2FA, please change the path to your profile (could automate)
    options = webdriver.ChromeOptions()
    options.add_argument(profile)

    # This is your chrome webdriver, please specify the path to it (cannot automate)
    driver = webdriver.Chrome(
        executable_path=path,
        chrome_options=options)

    # ImplicitWait waits up to a certain time for an element to appear before giving up (seconds)
    driver.implicitly_wait(10)

    # Get method gets the url on the browser
    driver.get("https://4554150-sb1.app.netsuite.com/app/center/card.nl?sc=-29&whence=")

    # Clicks on the "Login" button to log you in
    driver.find_element(By.ID, "login-submit").send_keys(Keys.ENTER)

    return driver


def loginFirefox(path, profile):
    # This loads your chrome profile to avoid 2FA, please change the path to your profile (could automate)
    password = ""
    firefoxProfile = webdriver.FirefoxProfile(profile)

    # This is your chrome webdriver, please specify the path to it (cannot automate)
    driver = webdriver.Firefox(
        executable_path=path, firefox_profile=firefoxProfile)

    # ImplicitWait waits up to a certain time for an element to appear before giving up (seconds)
    driver.implicitly_wait(10)

    # Get method gets the url on the browser
    driver.get("https://4554150-sb1.app.netsuite.com/app/center/card.nl?sc=-29&whence=")

    passwordBox = driver.find_element(By.ID, "password")

    if passwordBox.text == "":
        passwordBox.send_keys(password)

    # Clicks on the "Login" button to log you in
    driver.find_element(By.ID, "login-submit").send_keys(Keys.ENTER)

    return driver
