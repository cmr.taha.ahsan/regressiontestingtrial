# RegressionTestingTrial

This is our first attempt at automating some of the regression tests in NetSuite
This project may be superseded by a more complete version, but current learning of regression tests will be done here## Name
Regression Testing Trial

## Description
The aim of this project is for us to learn and be comfortable with Selenium and learn some regression testing. It may be that further down the line, this becomes more regulated than a simple git repo, but this is here for now so that there can be progress and every member has access to the latest files.

## Installation
Installation is detailed in the Udemy course, but for completeness, you will need to download python, install Selenium using pip, and download and extract Chrome Webdriver for the appropriate version

## Usage
To use this you will need to find your chrome webdriver path, and your chrome profile path. Specify those in IM400, and you should be able to log in easily.
Please ensure that you have the necessary chrome/firefox webdrivers downloaded and their directories specified, along with the password for firefox if doesn't seem to autofill your password. This setting can be found in the loginFirefox() function in Login.py

## Support
Talk to either Evan, Dennis, or Taha for support

## Roadmap
Future development will include more regression tests, unit testing capabilities, and give a bit more flow and structure to how the tests are executed

## Contributing
This project should only be visible to CMR Employees, and so you may be able to contribute, but I doubt you'd want to

## Authors and acknowledgment
Thank you to Jerry and Rizwan for guidance and for pushing us to develop this and be better :)

## License
I am not sure

## Project status
In development
